import sys
import argparse
from image import image_parse
import numpy as np
from PIL import Image
from plot import plot_func
import pickle

size = 50
epochs = 15

parser = argparse.ArgumentParser(description='Plot linear functions from images')
subparsers = parser.add_subparsers(help='List of commands', dest="command")
image_parser = subparsers.add_parser('image', help='Parse image and plot function from it')
image_parser.add_argument('path', action='store', help='Path to image')

learn_parser = subparsers.add_parser('learn', help='Learn NN from dataset')

if __name__ == "__main__":
    args = parser.parse_args()
    if args.command == "image":
        result = image_parse(np.array(Image.open(args.path).convert('L')), "labels",
              "model.h5", size)
        print("Parsed eq:", result)
        plot_func(result)
    elif args.command == "learn":
        from learn import learn
        di = learn("dataset.npz", "model.h5", size, epochs)

        di = {v: k for (k, v) in di.items()}
        with open("labels", 'wb') as fp:
            pickle.dump(di, fp)
