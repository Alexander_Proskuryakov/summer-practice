from PIL import Image, ImageDraw
import pickle
import numpy as np
from keras.models import load_model


def mark_connected(im, checked, x, y, ans):
    if im[x][y] == 0 or checked[x][y] != 0:
        return
    checked[x][y] = 1
    ans[-1].append(y)
    ans[-1].append(x)
    mark_connected(im, checked, x - 1, y, ans)
    mark_connected(im, checked, x + 1, y, ans)
    mark_connected(im, checked, x, y - 1, ans)
    mark_connected(im, checked, x, y + 1, ans)


def divide_by_connectivity(im):
    checked = np.zeros(im.shape)
    ans = []
    for i in range(np.size(im, 0)):
        for j in range(np.size(im, 1)):
            if im[i][j] != 0 and checked[i][j] == 0:
                ans.append([])
                mark_connected(im, checked, i, j, ans)
    return ans


class Symbol:
    maxy, miny, maxx, minx = 0, 0, 0, 0

    def height(self):
        return self.maxy - self.miny

    def width(self):
        return self.maxx - self.minx

    def middle_y(self):
        return (self.miny + self.maxy) / 2

    def middle_x(self):
        return (self.minx + self.maxx) / 2


def do_sqrt(symb, symlist):
    it = 0
    while it < len(symlist):
        if symlist[it].id == symb.id:
            it += 1
            continue
        if symlist[it].maxx <= symb.maxx and symlist[it].minx >= symb.minx \
                and symb.miny < symlist[it].middle_y() < symb.maxy:
            symb.bottom.append(symlist[it])
            del symlist[it]
        else:
            it += 1
    symb.str = "\\sqrt" + do_expr(symb.bottom)


def do_frac(symb, symlist):
    it = 0
    while it < len(symlist):
        if symlist[it].id == symb.id:
            it += 1
            continue
        if symlist[it].maxx <= symb.maxx and symlist[it].minx >= symb.minx:
            if symlist[it].middle_y() < symb.miny:
                symb.top.append(symlist[it])
            else:
                symb.bottom.append(symlist[it])
            del symlist[it]
        else:
            it += 1
    if len(symb.top) != 0 and len(symb.bottom) != 0:
        symb.str = "\\frac" + do_expr(symb.top) + do_expr(symb.bottom)
    else:
        symb.str = "-"


def do_int(symb, symlist):
    it = 0
    while it < len(symlist):
        if symlist[it].id == symb.id:
            it += 1
            continue
        if symlist[it].maxx <= symb.maxx + symb.width() / 2 and symlist[it].minx >= symb.minx - symb.width() / 2:
            if symlist[it].middle_y() < symb.miny:
                symb.top.append(symlist[it])
            elif symlist[it].middle_y() > symb.maxy:
                symb.bottom.append(symlist[it])
            else:
                it += 1
                continue
            del symlist[it]
        else:
            it += 1
    if len(symb.top) != 0 and len(symb.bottom) != 0:
        symb.str = "\\int\\limits_" + do_expr(symb.bottom) + "^" + do_expr(symb.top)
    else:
        symb.str = "\\int "


def do_pow(symb, symlist):
    it = symlist.index(symb)
    it += 1
    while it < len(symlist):
        if symlist[it].id == symb.id:
            it += 1
            continue
        it += 1


def do_expr(symlist):
    ans = ""
    it = 0
    while it < len(symlist):
        if symlist[it].name == "-":
            symlist[it].top = []
            symlist[it].bottom = []
            tmp = symlist[it]
            do_frac(symlist[it], symlist)
            it = symlist.index(tmp)
        it += 1
    it = 0
    while it < len(symlist):
        if symlist[it].name == "int":
            symlist[it].top = []
            symlist[it].bottom = []
            tmp = symlist[it]
            do_int(symlist[it], symlist)
            it = symlist.index(tmp)
        it += 1
    it = 0
    while it < len(symlist):
        if symlist[it].name == "sqrt":
            symlist[it].bottom = []
            tmp = symlist[it]
            do_sqrt(symlist[it], symlist)
            it = symlist.index(tmp)
        it += 1

    it = 0
    while it < len(symlist):
        if symlist[it].name.islower() or symlist[it].name.isnumeric() or symlist[it].name == ')':
            symlist[it].top = []
            symlist[it].bottom = []
            tmp = symlist[it]
            it += 1
            while it < len(symlist) and (tmp.middle_y() - 0.25 * tmp.height() > symlist[it].middle_y()
                                         or tmp.middle_y() + 0.25 * tmp.height() < symlist[it].middle_y())\
                    and tmp.height() > symlist[it].height():
                if tmp.middle_y() - 0.25 * tmp.height() > symlist[it].middle_y():
                    tmp.top.append(symlist[it])
                else:
                    tmp.bottom.append(symlist[it])
                del symlist[it]
            if len(tmp.bottom) != 0:
                tmp.str += "_" + do_expr(tmp.bottom)
            if len(tmp.top) != 0:
                tmp.str += "^" + do_expr(tmp.top)
            it -= 1
        ans += symlist[it].str
        it += 1
    return ans


def image_parse(im, dictpath, model_path, size):
    myim = im
    myim = (255 - myim) / 255

    symb = divide_by_connectivity(myim)
    counter = 0
    symlist = []
    for i in range(len(symb)):
        if len(symb[i]) < size:
            continue
        maxx = max(symb[i][::2])
        minx = min(symb[i][::2])
        maxy = max(symb[i][1::2])
        miny = min(symb[i][1::2])

        s = Symbol()
        s.minx = minx
        s.miny = miny
        s.maxx = maxx
        s.maxy = maxy

        if maxx - minx == maxy - miny == 0:
            continue

        if maxx - minx < maxy - miny:
            mult = float(size) / (maxy - miny)
            minx -= ((maxy - miny) - (maxx - minx)) / 2
        else:
            mult = float(size) / (maxx - minx)
            miny -= ((maxx - minx) - (maxy - miny)) / 2

        symb[i][::2] = [(n - minx) for n in symb[i][::2]]
        symb[i][1::2] = [(n - miny) for n in symb[i][1::2]]
        if max(maxx - minx, maxy - miny) > size:
            symb[i] = [int(n * mult) for n in symb[i]]
            dim = size
        else:
            dim = max(maxx - minx, maxy - miny)
        im = Image.new('RGB', (dim, dim), 0xFFFFFF)
        draw = ImageDraw.Draw(im)
        draw.point(symb[i], fill=0x0)
        if dim < size:
            im = im.resize((size, size))

        s.data = 1 * (np.array(im.convert('L')) < 128)
        s.id = counter
        symlist.append(s)
        counter += 1

    symlist = sorted(symlist, key=(lambda x: x.minx))

    with open(dictpath, 'rb') as fp:
        label_dict = pickle.load(fp)
    model = load_model(model_path)

    for symb in symlist:
            cl_symb = symb.data
            cl_symb = np.array([cl_symb[:, :, np.newaxis]])
            symb.name = label_dict[model.predict(cl_symb[0, np.newaxis]).argmax(axis=1)[0]]
            symb.str = symb.name
            if symb.str == 'infty':
                symb.str = '\\infty'
            elif symb.str == ')':
                symb.str = '\\right)'
            elif symb.str == '(':
                symb.str = '\\left('

    return do_expr(symlist)
