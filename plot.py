import matplotlib.pyplot as plt
import numpy as np
import sympy as sy

def plot_func(string):
    new_string = string[0]
    for i in range(1, len(string)):
        if "a" <= string[i] <= "z" or "A" <= string[i] <= "Z" and "0" <= string[i - 1] <= "9":
            new_string += "*x"
        else:
            new_string += string[i]
    func = sy.sympify(new_string)
    x = np.arange(0, 10, 0.1)

    plt.plot(x, [func.evalf(subs={"x": i}) for i in x])
    plt.title(string)
    plt.show()
