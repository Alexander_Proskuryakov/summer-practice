from keras.models import Sequential
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.layers.core import Activation, Flatten, Dense
from keras.optimizers import SGD
from keras.utils import np_utils
from sklearn.model_selection import train_test_split
import numpy as np

def learn(datapath, model_path, size, epochs):
    dataset = np.load(datapath)
    data = dataset['dataset_x']
    labels = dataset['dataset_y']
    labels_dict = {}
    i = 0
    for item in labels:
        if i > 0 and item in labels_dict:
            continue
        else:
            labels_dict[item] = i
            i += 1

    rng_state = np.random.get_state()
    np.random.shuffle(data)
    np.random.set_state(rng_state)
    np.random.shuffle(labels)
    labels = np.array([labels_dict[k] for k in labels])

    data = data[:, :, :, np.newaxis]

    (trainData, testData, trainLabels, testLabels) = train_test_split(data, labels, test_size=0.05)

    trainLabels = np_utils.to_categorical(trainLabels, i)
    testLabels = np_utils.to_categorical(testLabels, i)

    opt = SGD()
    width = size
    height = size
    classes = i
    model = Sequential()
    model.add(Convolution2D(20, 5, 5, border_mode="same", input_shape=(height, width, 1)))
    model.add(Activation("relu"))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

    model.add(Convolution2D(50, 5, 5, border_mode="same"))
    model.add(Activation("relu"))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

    model.add(Convolution2D(100, 5, 5, border_mode="same"))
    model.add(Activation("relu"))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

    model.add(Flatten())
    model.add(Dense(500))
    model.add(Activation("relu"))

    model.add(Dense(classes))
    model.add(Activation("softmax"))
    model.compile(loss="categorical_crossentropy", optimizer=opt, metrics=["accuracy"])

    model.fit(trainData, trainLabels, batch_size=128, nb_epoch=epochs, verbose=1)
    (loss, accuracy) = model.evaluate(testData, testLabels, batch_size=128, verbose=1)
    print("[INFO] accuracy: {:.2f}%".format(accuracy * 100))
    model.save(model_path)
    return labels_dict
